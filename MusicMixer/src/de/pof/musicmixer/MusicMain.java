package de.pof.musicmixer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

public class MusicMain {
	
	static ArrayList<String> Titles = new ArrayList<>();
	static ArrayList<Integer>Durations = new ArrayList<>();
	static BufferedWriter bw;
	
	/*
	 * 
	 * "Die Idee mag ich. Es ist vielleicht nicht vollst�ndig, aber w�rde es nicht reichen, 
	 *  wenn man zuf�llig arbeitet. Man testet dabei zwar nicht alle M�glichkeiten, 
	 *  kommt aber einfacher davon. Man berechnet erstmal den Durchschnittswert der Liste, 
	 *  dann kann man absch�tzen wie viele Lieder man ungef�hr braucht (das nennen wir mal n), 
	 *  um das Video zu f�llen.
	 *	Dann w�hlt man n-Lieder zuf�llig aus, addiert diese und speichert den Wert. 
	 *	Wenn man das so ca. 100 Mal (das kannst du auch ver�ndern, je nachdem wie lange das braucht, 
	 *	je �fters desto genauer)gemacht hat, sollten schon recht gute Ergebnisse herauskommen. 
	 *	Die Zufallszahlen solltest du noch vorher �berpr�fen, damit du nicht einmal das gleiche Lied 
	 *	zweimal miteinberechnest." ~Ich05
	 * 
	 *  Download: https://www.dropbox.com/s/u6dr6k30yfmjtsm/MusicMixer.jar?dl=0
	 */
	
	public static void main(String[] args){
		File f = new File("Music.txt");
		if(!f.exists()){
			try {
				f.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		loadMusic(f);
			
		Scanner s = new Scanner(System.in);
		System.out.println("What do you want?");
		System.out.println("[0] Add a music file");
		System.out.println("[1] Create a music mix");
		System.out.println("[2] Show all Tracks");
		
		int methode = Integer.parseInt(s.nextLine());
		if(methode == 0){
			
			System.out.println("So you want to add a music file? Tell me title");
			String title = s.nextLine();
			System.out.println("Great! And now the Duration");
			int dura = Integer.parseInt(s.nextLine());
			Titles.add(title);
			Durations.add(dura);
			s.close();
		}else if(methode == 1){
			System.out.println("Tell me the lenght of you video");
			int lenght = Integer.parseInt(s.nextLine());
			s.close();
			int vids = (int)((float)lenght/middleTime());
			
			ArrayList<String> usedTitles = new ArrayList<>();
			int time = 0;
			
			for(int i = 0; i < 10; i++){
				
				ArrayList<String> testTitles = new ArrayList<>();
				int testTime = 0;
				Random r = new Random();
				
				if(vids > 1){
					for(int a = 0; a < vids; a++){
						int pointer = r.nextInt(Titles.size());
						if(stringInList(Titles.get(pointer), testTitles)){
							if(Titles.size() < a){
								
							}else a--;
						}else{
							testTime += Durations.get(pointer);
							testTitles.add(Titles.get(pointer));
						}
						if(testTitles.size() == Titles.size())break;
					}
				}else{
					int bestTitle = -1;
					for(int a = 0; a < Titles.size(); a++){
						if(bestTitle < 0)bestTitle = a;
						if(Math.abs(lenght - Durations.get(a))<Math.abs(lenght-Durations.get(testTime))){
							bestTitle = a;
						}
					}
					testTitles.add(Titles.get(bestTitle));
					usedTitles = testTitles;
				}
				
				if(Math.abs(lenght-time)>Math.abs(lenght-testTime)){
					time = testTime;
					usedTitles = testTitles;
				}
			}
			System.out.println("You should use this titles: ");
			for(String a: usedTitles){
				System.out.println(a);
			}
			
		}else if(methode == 2){
			for(int i = 0; i < Titles.size(); i++){
				System.out.println(Titles.get(i) + "-" + Durations.get(i));
			}
			
		}
		s.close();
		
		
		
		saveTracks(f);
	}
	
	public static float middleTime(){
		int dura = 0;
		for(int i = 0; i < Titles.size(); i++){
			dura += Durations.get(i);
		}
		return dura/Titles.size();
	}
	
	
	public static void loadMusic(File f){
		FileReader fr = null;
		try {
			fr = new FileReader(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		BufferedReader br = new BufferedReader(fr);
		
		String line = null;
		try {
			line = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		while(line != null){
			String[] contents = line.split("-");
			
			Titles.add(contents[0]);
			Durations.add(Integer.parseInt(contents[1]));
			
			try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void saveTracks(File f){
		
		FileWriter fw = null;
		try {
			fw = new FileWriter(f);
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		bw = new BufferedWriter(fw);
		
		for(int i = 0; i < Titles.size(); i++){
			try {
				bw.write(Titles.get(i) + "-" + Durations.get(i) + "\n");
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				bw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}	
	
	public static void loadFile(File f){
		AudioInputStream audioInputStream = null;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(f);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		AudioFormat format = audioInputStream.getFormat();
		long frames = audioInputStream.getFrameLength();
		double durationInSeconds = (frames+0.0) / format.getFrameRate();  
		System.out.println(durationInSeconds);
	}
	
	public static boolean stringInList(String s, ArrayList<String>StringList){
		for(String a: StringList){
			if(a.equals(s))return true;
		}
		return false;
	}
	
}